cloud_data=PATH
repo_path=PATH

cd {repo_path}/src

#  Build splicing module database from somatic AML-associated genes
python MakeSplicingModuleDatabase.py -c ${repo_path}/config_aml

# Query a set of splicing modules of interest within the CD34 MAJIQ-PSI database
python QuerySplicingModuleDatabase.py -c ${repo_path}/config_cd34 -moi ${cloud_data}/MAJIQ/cpd_noir_mr3_dr3_ns/most_variable_splicing_modules_cp.csv

# Produce pairwise correlation matrices for each of the splicing module databases
python SplicingModuleCoregulation.py -c ${repo_path}/config_aml -m METHOD -i MODULE_ID
python SplicingModuleCoregulation.py -c ${repo_path}/config_aml -m METHOD -i MODULE_ID

# Unpack complex splicing module representing EZH2 exon 11 and 12 skipping
python UnpackSplicingModule.py -c ${repo_path}/config_aml -sm MODULE_ID

# Build splicing module from familial-AML associated genes
python MakeSplicingModuleDatabase.py -c ${repo_path}/config_aml

# Correlate the splicing modules with expression of the related genes
python CorrSplicingModulesWithexpression.py -c ${repo_path}/config_aml

# Correlate the expression of NMD targets with DHX34 poison exon 12b inclusion
python CorrSplicingModuleWithGeneset.py -c ${repo_path}/config_aml -sm chr19:47377100-47377206 -g ${repo_path}/features/nmd_targets.csv -n NMD