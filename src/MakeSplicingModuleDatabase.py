"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by: David Wang

Description: Pipeline used to identify  produce splicing
modules from a collection of local splicing variations
quantified by the MAJIQ-PSI algorithm.

##################################################################
"""

import argparse
import configparser
import os.path
from internals import majiq_tools as majiq


def MakeSplicingModuleDatabase(genelist, build_path, groups, refgroup, qr_thresh):
    """
    Pipeline to make splicing module database from MAJIQ-psi files within a defined NAJIQ-build path.
    :param genelist: list of genes extracted from MAJIQ psi quantifications.
    :param build_path: string. path containing the MAJIQ-build (splicegraph) and MAJIQ-psi output.
    :param groups: dictionary containing group label keys and sample name elements.
    :param refgroup: name of reference group used for comparative analysis
    :param qr_thresh: quantification ratio (qr) threshold. (qr = samples_where_lsv_is_quantified / all_samples_in_group)
    :return:
    """
    print("Querying MAJIQ BUILD:", os.path.dirname(build_path))
    # Get list of PSI .tsv files in user-defined path.
    psifiles = majiq.listPSIfiles(build_path=build_path, groups=groups)

    # Build LSV database (LSV-DB) from the MAJIQ-PSI files for every sample in dataset.
    lsvdb = majiq.ConstructLSVDatabaseFromMAJIQPSI(psifiles=psifiles, genes=genelist, groups=groups)

    # Construct Splicing Module Database using the MAJIQ-PSI LSV database.
    moduledb = majiq.ConstructSplicingModuleDatabase(lsvdb=lsvdb, refgroup=refgroup, groups=groups, qr_thresh=qr_thresh)

    # Construct Feature Table from Splicing Module Database
    smdbext = majiq.ModuleDBFeatureExtraction(module_dic=moduledb, lsv_dic=lsvdb, groups=groups)

    # Determine Most Variable Splicing Modules
    mvsmdf = majiq.MostVariableSplicingModules(smdb=smdbext, ref_group=refgroup)
    return mvsmdf, smdbext, lsvdb


if __name__ == '__main__':
    # Command Line Arguments
    parser = argparse.ArgumentParser(description='Make splicing module database.')
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    args = parser.parse_args()

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    build_path = config['DEFAULT']['build_path']
    genelist = [i.strip("\n") for i in open(config['DEFAULT']['genelist'], 'r')]
    qr_thresh = float(config['DEFAULT']['qr_thresh'])
    groups = dict(config._sections['GROUPS'])
    refgroup = config['DEFAULT']['refgroup']
    splicedb = config['DEFAULT']['splicedb']

    # Generate a database of splicing modules based on a defined reference group.
    mvsmdf, moduledbext, lsvdb = MakeSplicingModuleDatabase(genelist, build_path, groups, refgroup, qr_thresh)

    # Export Most-Variable Splicing Module Database
    output1 = '{}/{}'.format(build_path, splicedb)
    mvsmdf.to_csv(output1, index=True, index_label='MODULE_ID')

    # Export Extended Splicing Module Database
    output2 = '{}/extended_{}'.format(build_path, splicedb)
    moduledbext.to_csv(output2, index=True, index_label='MODULE_ID')
