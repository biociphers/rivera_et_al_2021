"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by: David Wang

Description: Query defined set of LSVs within a splicing module database

##################################################################
"""

import pandas as pd
import argparse
import configparser
from MakeSplicingModuleDatabase import MakeSplicingModuleDatabase
from internals import majiq_tools as majiq


def QuerySplicingModuleDatabase(lsvdb, moi, groups):
    """
    :param lsvdb: LSV database object with MAJIQ-PSI information for the
    :param moi: splicing modules of interest to query within the LSV database
    :param groups: groups to query within LSV database
    :return:
    """
    junctions_of_interest = list()  # Get junctions of interest from modules of of interest
    for j in list(moi["JUNCTION_ID"]):
        coord = "{}:{}".format(j.split(":")[0], j.split(":")[1])
        junctions_of_interest.append(coord)
    lsvs_of_interest = list()  # Get LSVs of interest from junctions of interest
    for e in lsvdb:
        junctions_e = lsvdb[e]["JUNCTIONS"]
        coords = list()
        for j in junctions_e:
            coord = "{}:{}".format(j.split(":")[0], j.split(":")[1])
            coords.append(coord)
        check = any(item in coords for item in set(junctions_of_interest))
        if check is True:
            lsvs_of_interest.append(e)
            print("Found LSV:", e, "containing junction", j)
    lsvdbfilt = {e: lsvdb[e] for e in lsvs_of_interest}  # Filter LSV-DB for LSVs of interest

    # Make Reference-adjusted splicing module database
    moduledb = majiq.ConstructSplicingModuleDatabase(lsvdb=lsvdbfilt, refgroup=refgroup, groups=groups, qr_thresh=0)

    # Construct Feature Table from Splicing Module Database
    smdb = majiq.ModuleDBFeatureExtraction(module_dic=moduledb, lsv_dic=lsvdb, groups=groups)
    return smdb


if __name__ == '__main__':
    # Command Line Arguments
    parser = argparse.ArgumentParser(description='Query splicing modules in database.')
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    parser.add_argument("-moi", "--splice_mod", help="Splicing module database table with the modules of interest",
                        metavar="FILE")
    args = parser.parse_args()

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    build_path = config['DEFAULT']['build_path']
    build_name = config['DEFAULT']['build_name']
    genelist = [i.strip("\n") for i in open(config['DEFAULT']['genelist'], 'r')]
    qr_thresh = float(config['DEFAULT']['qr_thresh'])
    groups = dict(config._sections['GROUPS'])
    refgroup = config['DEFAULT']['refgroup']

    # Generate a database of splicing modules based on a defined reference group.
    mvsmdf, moduledbext, lsvdb = MakeSplicingModuleDatabase(genelist, build_path, build_name,
                                                            groups, refgroup)
    # Query database
    moi = pd.read_csv(args.moi)
    moduledbft = QuerySplicingModuleDatabase(lsvdb, moi, groups)
    output2 = '{}/{}/{}_query_splicing_modules.csv'.format(build_path, build_name, refgroup)
    moduledbft.to_csv(output2, index=True, index_label='MODULE_ID')
