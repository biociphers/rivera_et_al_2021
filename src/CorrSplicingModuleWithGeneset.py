"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by:

Description: Pipeline to determine gene expression variation

##################################################################
"""

import argparse
import configparser
import pandas as pd
import scipy.stats as stats
from functools import reduce
from statistics import stdev, mean
import random
from internals.majiq_tools import importxsets


def CorrSplicingModulesithGeneset(xsets, smdb, sm, geneset):
    """
    Correlate expression of genes within a defined geneset with the psi values of a splicing module.
    :param xsets: gene expression dataframe
    :param smdb: splicing module database
    :param sm: splicing module to correlate (MODULE_ID)
    :param geneset: set of genes to correlate with splicing module
    :return:
    """
    geneset = geneset.loc[:, ['GENE_ID', 'GENE_NAME']]
    xset_psi_realcors = list()
    gene_ids = list(geneset['GENE_ID'])
    for k in xsets.keys():
        xset_filt = xsets[k][xsets[k].index.isin(gene_ids)]
        xset_samples = list(xset_filt.columns)
        xset_filt = xset_filt[xset_samples].transpose()

        # Get samples for which splicing module m was quantified.
        smv = smdb.loc[sm]
        psi_quant = smv.reindex(xset_samples).dropna().index
        psi = smdb.loc[sm, psi_quant]  # real psi value vector

        # Correlate real value dataset
        cors = list()  # initialize empty list to store correlations
        for g in xset_filt.loc[xset_samples].columns:
            exprs = xset_filt.loc[psi_quant, g]
            pearson_test_real = stats.pearsonr(psi, exprs)
            cors.append([g, pearson_test_real[0], pearson_test_real[1]])
        realcorsdf = pd.DataFrame(cors, columns=['GENE_ID', 'PEARSON_R_{}'.format(k), 'PVALUE_{}'.format(k)])
        xset_psi_realcors.append(realcorsdf)
    stats_dic = dict({'NUM_XPRESD': [len(k) for k in xset_psi_realcors]})  # store number of expressed genes per group
    # Merge real value datasets into single correlation dataframe
    cors_rv = reduce(lambda x, y: pd.merge(x, y, on='GENE_ID', how='outer'), xset_psi_realcors)
    cors_rv = cors_rv.dropna()
    # Add GENE_NAME to correlation dataset
    g_t = geneset[geneset.GENE_ID.isin(cors_rv.GENE_ID)]  # filter geneset for for genes within the cors dataframe
    cors_rv = cors_rv.merge(g_t, on='GENE_ID', how='outer')  # merge filtered geneset with the cors dataframe
    return cors_rv, stats_dic


def CorrSplicingModulesithGenesetRandomization(xsets, smdb, sm, n_random_genes, boot):
    """
    Correlate expression of genes within a defined geneset with the psi values of a splicing module.
    :param xsets: gene expression dataframe
    :param smdb:  splicing module database
    :param sm:  splicing module to correlate (MODULE_ID)
    :param n_random_genes: number of random genes to source from expression set
    :return:
    """
    xset_names = [k for k in xsets.keys()]
    xset_psi_bootstraps = list()
    boot_rand = 0  # Bootstrap Randomization
    boot_max = boot  # Maximum number of bootstraps

    while boot_rand <= boot_max:
        print("bootstrapping...{}".format(boot_rand))
        # Random Sample Geneset from input expression set.
        geneset_random = random.sample(list(xsets[list(xsets.keys())[0]].index), k=n_random_genes)

        xset_psi_b = list()
        # Calculate Randomized Pearson Correlations for each Xset
        for k in xsets.keys():
            xset_filt = xsets[k][xsets[k].index.isin(geneset_random)]
            xset_samples = list(xsets[k].columns)

            # Get samples for which splicing module m was quantified.
            psi_quant = smdb.loc[sm].reindex(xset_samples).dropna().index
            psi = smdb.loc[sm, psi_quant]  # real psi value vector

            # Correlate random value dataset
            cors = list()  # initialize empty list to store correlations
            txset = xset_filt[xset_samples].transpose()  # transpose xset to match psi structure
            for g in txset.loc[xset_samples].columns:
                exprs = txset.loc[psi_quant, g]
                pearson_test_rand = stats.pearsonr(psi, exprs)
                cors.append([g, pearson_test_rand[0], pearson_test_rand[1]])
            # Make pearson test correlation dataframe
            corsdf = pd.DataFrame(cors, columns=['GENE_ID',
                                                 'PEARSON_R_{}_{}'.format(boot_rand, k),
                                                 'PVALUE_{}_{}'.format(boot_rand, k)])
            xset_psi_b.append(corsdf)  # add pearson dataframe to list of cohort dataframes for bootstrap b

        # Merge cohort correlations for bootstrap b in to single df.
        cors_dbf = reduce(lambda x, y: pd.merge(x, y, on='GENE_ID', how='outer'), xset_psi_b)
        # cors_dbf = cors_dbf.set_index('GENE_ID')
        cors_dbf = cors_dbf.dropna()

        # Append correlation df for bootstrap b to list storing all bootstrapped correlation dfs
        xset_psi_bootstraps.append(cors_dbf)
        boot_rand = boot_rand + 1

    # For each bootstrapped randomized correlation dataframe,
    # get number of genes with p < 0.05 in both datasets
    boot_sig = dict({'SIG': list(), 'UPREG': list(), 'DOWNREG': list()})
    boot = 0
    while boot <= boot_max:
        cors_boot = xset_psi_bootstraps[boot]
        query_string = " & ".join(['PVALUE_{}_{} <= 0.05'.format(boot, id) for id in xset_names])
        coors_boot_sig = cors_boot.query(query_string)
        boot_sig['SIG'].append(len(coors_boot_sig.index))

        query_string_pos = " & ".join(['PEARSON_R_{}_{} > 0'.format(boot, id) for id in xset_names])
        up_sig = coors_boot_sig.query(query_string_pos)
        boot_sig['UPREG'].append(len(up_sig.index))

        query_string_neg = " & ".join(['PEARSON_R_{}_{} < 0'.format(boot, id) for id in xset_names])
        down_sig = coors_boot_sig.query(query_string_neg)
        boot_sig['DOWNREG'].append(len(down_sig.index))

        boot = boot + 1

    # Average number of genes with p < 0.05 for both datasets across all bootstrapped randomization
    boot_mean_sig = mean(boot_sig['SIG'])
    boot_stdev_sig = stdev(boot_sig['SIG'])  # STDEV for the average number of significant genes across all permutations

    boot_mean_sig_pos = mean(boot_sig['UPREG'])
    boot_stdev_sig_pos = stdev(boot_sig['UPREG'])

    boot_mean_sig_neg = mean(boot_sig['DOWNREG'])
    boot_stdev_sig_neg = stdev(boot_sig['DOWNREG'])

    stats_dic = {'all_sig': [boot_mean_sig, boot_stdev_sig],
                 'positive': [boot_mean_sig_pos, boot_stdev_sig_pos],
                 'negative': [boot_mean_sig_neg, boot_stdev_sig_neg]}
    return xset_psi_bootstraps, stats_dic


if __name__ == '__main__':
    # Command Line Arguments
    parser = argparse.ArgumentParser(description='Correlate a defined splicing module with a geneset.')
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    parser.add_argument("-sm", "--splice_mod", help="Single splicing module to correlate with geneset",
                        metavar="STR")
    parser.add_argument("-g", "--geneset", help="Geneset table with genes to query from xsets", metavar="FILE")
    parser.add_argument("-n", "--name", help="Name of geneset", metavar="STR")
    parser.add_argument("-b", "--boot", nargs='?', default=100, help="Number of bootstraps for randomization",
                        metavar="INT")
    parser.add_argument("-xpt", "--express_thresh", nargs='?', default=1, help="Threshold of gene expression",
                        metavar="INT")
    args = parser.parse_args()

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    build_path = config['DEFAULT']['build_path']
    splicedb = pd.read_csv('{}/{}'.format(build_path, config['DEFAULT']['splicedb'])).set_index('MODULE_ID')
    xsets = importxsets(xsets=dict(config._sections['XSETS']), xprs_thresh=args.express_thresh)  # import expression set
    geneset = pd.read_csv(args.geneset)  # Read geneset csv file
    name = args.name  # Name of geneset
    sm = args.splice_mod  # Define splicing module of interest to correlate
    b = args.boot
    gene = str(splicedb.loc[sm].GENE_NAME)  # Get gene name for the splicing module
    print("Geneset Correlation Analysis for splicing module {} in gene {}.".format(sm, gene))

    # Correlate Splicing Module of interest with expression of genes denoted within geneset
    cors_df, stats_dic_realcor = CorrSplicingModulesithGeneset(xsets, splicedb, sm, geneset)
    print(stats_dic_realcor)
    cors_df.to_csv('{}/{}_splicing_module_{}_geneset_correlation.csv'.format(build_path, gene, name))

    # Perform correlations with a randomly generated geneset of interest to account for multiple hypothesis testing
    n_genes = int(stats_dic_realcor['NUM_XPRESD'][0])
    boot_dfs, stats_dic_random = CorrSplicingModulesithGenesetRandomization(xsets, splicedb, sm,
                                                                            n_random_genes=n_genes, boot=b)
    print(stats_dic_random)  # Print randomization mean statistics.
    # Todo Append statistics on top of cors_df table
