"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by: __pending_revision__

Description: Export Table of underlying LSVs and Junctions for an
input splicing module within a smdb object.
##################################################################
"""

import argparse
import configparser
import pandas as pd
from internals import majiq_tools as majiq


def UnpackSplicingModuleJunctions(sm, smdb, ref_build, groups):
    print("Querying MAJIQ BUILD:", ref_build)
    genes = set(smdb.GENE_NAME)

    # Get list of PSI .tsv files in user-defined path.
    psifiles = majiq.listPSIfiles(build_path=build_path, groups=groups)

    # Extract LSV information for the gene of interest from the list of MAJIQ-PSI files.
    lsvdb = majiq.ConstructLSVDatabaseFromMAJIQPSI(psifiles=psifiles, genes=genes, groups=groups)

    print("Generating Module-Level PSI Dataframe...")
    # Initialize Junction vector extraction for  Splicing Module sm.
    smlsvs = [lsv.strip(" '") for lsv in smdb.loc[sm].loc['SPLICING_MODULE'].strip("{}").split(",")]
    module_psi_dic = dict()
    # For each Splicing Module
    for e in smlsvs:
        # List of Junctions for LSV e. Indexes of this list will be
        junctions_e = list(lsvdb[e]['JUNCTIONS'].keys())
        for j in junctions_e:
            j_idx = junctions_e.index(j)
            j_psi = lsvdb[e]['PSI'].loc[all_samples][j_idx]
            module_psi_dic.update({j: dict()})  # Initialize info_dict per JUNCTION_ID (unique identifier)
            # Add PSI value of junction J for sample s within LSV e in Module m
            for s in j_psi.index:
                module_psi_dic[j].update({s: j_psi[s]})
    module_psi_df = pd.DataFrame(module_psi_dic)
    return module_psi_df


if __name__ == '__main__':
    # Command Line Arguments
    parser = argparse.ArgumentParser(description='Unpack junction data from a splicing module.')
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    parser.add_argument("-sm", "--splicing_module", help="Specify splicing module ID to unpack", metavar="STR")
    args = parser.parse_args()

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    build_path = config['DEFAULT']['build_path']
    groups = dict(config._sections['GROUPS'])
    splicedb = pd.read_csv('{}/{}'.format(build_path, config['DEFAULT']['splicedb'])).set_index('MODULE_ID')
    sm = args.splicing_module

    # Query LSV junction data for particular junction
    juncdf = UnpackSplicingModuleJunctions(sm=sm, smdb=splicedb, ref_build=build_path, groups=groups)
    outpath = build_path
    output = '{}/psi_{}.csv'.format(outpath, sm)
    juncdf.to_csv(output, index=True, index_label='MODULE_ID')
