"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by:

Description: Function to Correlate Splicing Module Database with gene expression set.

##################################################################
"""

import argparse
import configparser
import scipy.stats as stats
import pandas as pd
from internals.majiq_tools import importxsets


def CorrSplicingModulesWithExpression(xset, smdb, g):
    print("Group: ", g)
    genedb = dict()
    xset = xsets[g]
    refsamples = [line.strip('\n') for line in open(groups[g])]
    for index, row in smdb.iterrows():
        # Extract information from rows
        junc = row['JUNCTION_ID']  # Get Junction j representing splicing module m
        module = row['MODULE_ID']
        gene = row['GENE_NAME']
        gene_id = row["GENE_ID"]
        psi = row[refsamples]
        # Add information to dictionary
        if gene not in genedb.keys():
            genedb.update({gene : dict()})
            genedb[gene].update({"ASMs": dict()})
            genedb[gene].update({"GENE_ID": gene_id})
            genedb[gene]["ASMs"].update({module: dict()})
            genedb[gene]["ASMs"][module].update({"PSI": psi})
            genedb[gene]["ASMs"][module].update({"JUNCTION": junc})
        else:
            genedb[gene]["ASMs"].update({module: dict()})
            genedb[gene]["ASMs"][module].update({"PSI": psi})
            genedb[gene]["ASMs"][module].update({"JUNCTION": junc})

    # Query genes associated with splicing modules within the rpkm database and add rpkm values to gene database.
    for gene in genedb.keys():
        gene_id = genedb[gene]["GENE_ID"]
        rpkm = xset.loc[xset.index == gene_id][refsamples]
        genedb[gene].update({'RPKM': rpkm.T.squeeze()})

    corr_l = list()  # Create list to store lists of correlations between splicing modules and gene expression.
    for g in genedb:  # For each gene gene g, calculate the cor btwn psi for splicing module m and rpkm for gene g.
        print('Calculating Correlations between Splicing-Modules and Gene Expression for gene {}...'.format(g))
        for sm in genedb[g]["ASMs"].keys():  # For every splicing module m in gene g
            junc = genedb[g]["ASMs"][sm]["JUNCTION"]
            # Dataframe of PSI for splicing module sm
            psi = pd.to_numeric(genedb[g]["ASMs"][sm]["PSI"]).to_frame(name="PSI")
            rpkm = pd.to_numeric(genedb[g]["RPKM"]).to_frame(name="RPKM")  # Dataframe of RPKM for gene g
            samples = list(psi.dropna().index)
            spearman_test = stats.spearmanr(psi.loc[samples], rpkm.loc[samples])
            pearson_test = stats.pearsonr(psi.loc[samples], rpkm.loc[samples])
            corr_v = [g, sm, junc,
                      abs(float(spearman_test[0])), float(spearman_test[1]),
                      abs(float(pearson_test[0])), float(pearson_test[1])]
            corr_v = corr_v + [rp[0] for rp in rpkm.values] + [p[0] for p in psi.values]
            corr_l.append(corr_v)
    cols_stats = ["GENE", "ASM", "JUNCTION", "spearmanr", "spearmanr_pvalue", "pearsonr", "pearsonr_pvalue"]

    # Convert Database to Dataframe
    cols_psi = ["{}.psi".format(sample) for sample in refsamples]
    cols_rpkm = ["{}.rpkm".format(sample) for sample in refsamples]
    cols_all = cols_stats + cols_rpkm + cols_psi
    corrs_df = pd.DataFrame(corr_l, columns=cols_all)  # Convert lists of lists to pandas dataframe
    return corrs_df


if __name__ == '__main__':
    # Command Line Arguments
    parser = argparse.ArgumentParser(description="Correlate PSI of a set of splicing modules with the"
                                                 " expression of the associated genes.")
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    args = parser.parse_args()

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    config.read("config")
    build_path = config['DEFAULT']['build_path']
    build_name = config['DEFAULT']['build_name']
    groups = dict(config._sections['GROUPS'])
    splicedb = pd.read_csv('{}/{}/{}'.format(build_path, build_name, config['DEFAULT']['splicedb']))
    xsets = importxsets(xsets=dict(config._sections['XSETS']), xprs_thresh=1)  # import expression set

    # Perform correlation analysis of splicing modules in database with expression of related genes.
    for g in groups.keys():
        cors_df = CorrSplicingModulesWithExpression(xsets, splicedb, g)
        cors_df.to_csv('{}/sm_xset_correlations_{}.csv'.format(build_path+build_name, g))
