"""
####################### ~ BIOCIPHERS ~ ###########################
Author: Osvaldo Rivera
Revised by:

Description: Produce pairwise matrix of correlations between splicing
             module features (samples or modules).

##################################################################
"""

import configparser
import argparse
import numpy as np
import pandas as pd
from scipy.stats import spearmanr
from scipy.stats import pearsonr


def PairwiseCorrlation(splicedb, samples, clustax, method, index, pval):
    """
    Create a correlation matrix of select samples or splicing modules from an input database.
    :param splicedb: splicing module database compiled with MakeSplicingModuleDatabase.py
    :param samples: samples selected for clustering from splicing module database.
    :param indices: splicing module IDs to use from the splicing module database (currently set default to all IDs).
    :param clustax: pairwise clustering of either "samples" or "splicing modules" axis.
    :param pval: boolean to indicate weather to calculate pvalue matrix.
    :return:
    """
    # Default clustering axis is 'feature':
    indices = list(splicedb[index])  # defines indices as splicing module names in splicedb
    df1 = splicedb[samples].transpose()
    df2 = splicedb[samples].transpose()
    if clustax is 'samples':
        df1 = splicedb[samples].dropna(axis=1, thresh=0.25*len(df1.keys()))
        df2 = splicedb[samples].dropna(axis=1, thresh=0.25*len(df2.keys()))
        indices = list(k for k in df1.keys())  # re-defines indices as sample names that passed the dropna threshold
    print('Calculating Pairwise Correlation using data from samples: {}...'.format(samples))
    # Initialize empty matrices: coeffmat=correlations; pvalmat=significance
    coeffmat = np.zeros((df1.shape[1], df2.shape[1]))
    pvalmat = np.zeros((df1.shape[1], df2.shape[1]))
    for i in range(df1.shape[1]):
        for j in range(df2.shape[1]):
            if i == j:  # Introduce nan if correlating same module
                coeffmat[i, j] = 'nan'
                pvalmat[i, j] = 'nan'
                continue
            # If correlating with a different module calculate correlation
            else:
                corrtest = 0  # Initialize variable
                if method == 'spearman':
                    corrtest = spearmanr(df1[df1.columns[i]], df2[df2.columns[j]], nan_policy='omit')
                elif method == 'pearson':
                    corrtest = pearsonr(df1[df1.columns[i]], df2[df2.columns[j]])
                else:
                    RuntimeError("Define statistical method")
                coeffmat[i, j] = abs(corrtest[0])  # Take Absolute Value of Correlation
                pvalmat[i, j] = corrtest[1]
    # Reindex correlation matrix using provided indices
    dfcoeff = pd.DataFrame(coeffmat, columns=df2.columns, index=df1.columns)
    dfcoeff = dfcoeff.reindex(indices)
    dfcoeff = dfcoeff.transpose()
    dfcoeff = dfcoeff.reindex(indices)
    # Reindex pvalue matrix using provided indices
    dfpvals = pd.DataFrame(pvalmat, columns=df2.columns, index=df1.columns)
    dfpvals = dfpvals.reindex(indices)
    dfpvals = dfpvals.transpose()
    dfpvals = dfpvals.reindex(indices)

    n = len(samples)
    bins = [0, 0.001/n, 0.01/n, 0.05/n, 1]  #Bonferroni Corrected P-value Cutoffs ##
    names = ['***', '**', '*', 'NS']
    if pval==True:
        pvals = dict()
        for m in dfpvals:
            pval_sig_m = pd.cut(dfpvals[m], bins=bins, labels=names)
            pvals.update({m: pval_sig_m})
        dfpvals_sig = pd.DataFrame(pvals)
    else:
        dfpvals_sig = 0
    return dfcoeff, pval, dfpvals_sig


if __name__ == '__main__':
    # Argument Parser
    parser = argparse.ArgumentParser(description='Pairwise comparison of splicing modules')
    parser.add_argument("-c", "--conf_file", help="Specify config file", metavar="FILE")
    parser.add_argument("-m", "--method", help="Specify method", metavar="STR")
    parser.add_argument("-a", "--axis", help="Specify clustering axis", metavar="STR")
    parser.add_argument("-i", "--index", help="Specify index", metavar="STR")
    # Todo implement exclusion list
    parser.add_argument("-e", "--exclude", help="Specify splicing modules to exclude from correlation", metavar="FILE")
    # Todo implement Module_ID to Module_Name translation file
    args = parser.parse_args()
    clustax = args.axis
    method = args.method
    index = args.index

    # Import Configuration Variables
    config = configparser.ConfigParser()
    config.read(args.conf_file)
    build_path = config['DEFAULT']['build_path']
    build_name = config['DEFAULT']['build_name']

    # Input Splicing module DB an
    splicedb = pd.read_csv('{}/{}/{}'.format(build_path, build_name, config['DEFAULT']['splicedb']))
    splicedb = splicedb.set_index(index, drop=False)
    groups = dict(config._sections['GROUPS'])
    for k in groups.keys():
        groups[k] = [line.strip("\n") for line in open(groups[k], 'r')]

    # Produce pairwise correlation matrix of features (splicing modules or samples)
    for k in groups.keys():
        samples = groups[k]
        pwsmcor, pval, pvalcor = PairwiseCorrlation(splicedb, samples, clustax, method, index, True)
        output_a = '{}/{}/{}_{}_pairwise_correlations_{}_copy.csv'.format(build_path, build_name, method, clustax, k)
        pwsmcor.to_csv(output_a)
        if pval is True:
            output_b = '{}/{}/{}_{}_pairwise_correlations_pval_{}_copy.csv'.format(build_path, build_name, method, clustax, k)
            pvalcor.to_csv(output_b)
        else:
            continue
