
# Splicing Variation Contributes to Functional Dysregulation of AML-associated Genes
Rivera et al. 2021, University of Pennsylvania

## I. Summary
The following README contains the code used to perform the analysis of highly variable splicing events across distinct acute myeloid leukemia (AML) patient cohorts. Most genes associated with AML are mutated in less than 10% of patients, suggesting that alternative mechanisms of gene disruption contribute to this disease. Using this pipeline, we find a set of splicing events that alter the expression of a subset of AML-associated genes independent of known somatic mutations. In comparison to CD34+ healthy donor cells, the splicing events identified via this pipeline express significant variability across the AML patient cohorts. The pipeline described here extracts the most variable splicing events quantified by the MAJIQ-PSI pipeline. Importantly, the pipeline implements the matching of overlapping junctions to addresses the data redundancy of LSVs quantified by the MAJIQ framework. The pipeline includes a series of scrips to concatenate MAJIQ-PSI files into a 'splicing module' database that summarizes the splicing quantification data and produces relevant statistics. We also include additional scripts to handle the (1) pairwise comparison of splicing modules within a defined database, (2) the exploration of underlying LSV features that make up a defined splicing module, (3) the extraction of a defined set of splicing modules from a separately built MAJIQ-PSI database, and (4) the robust correlation of splicing module quantification with gene expression measurements.   

## II. Splicing Module Quantification Method
### Setup
The tools and pipelines provided in this publication rely on packages distributed through Anaconda Cloud. The following code snippet installs every package dependency within a .yml file through the Conda environment.

```
conda env create -f splicedec.yml
```

### Data download
The pipeline described here works with your own set of MAJIQ builds, and can be configured according to the instructions laid out in this document. The data used for the exploration of splicing variations in AML can be downloaded here [DOWNLOAD LINK]. The 

### Splicing Module Pipeline and MAJIQ Data Manipulation
The splicing module pipeline described in Rivera et. al 2021 begins by configuring a set of key parameters. The following is an example config file.
```text
[DEFAULT]
build_path = /Volumes/OZCO/Dropbox/Biociphers/cloud_data/MAJIQ/cpd_noir_mr3_dr3_ns
genelist = /Users/admin/Biociphers/rivera_et_al_2021/features/amlgenes
refgroup = pennaml
qr_thresh = 0.8
splicedb = module_database_original_canonical

[GROUPS]
pennaml = /Users/admin/Biociphers/rivera_et_al_2021/features/penn_aml_patients.txt
beataml = /Users/admin/Biociphers/rivera_et_al_2021/features/beataml_samples.txt
```
The pipeline uses configuration files <CONFIG_FILE> called with ( -c/--conf ) throughout the scripts. This file should define the MAJIQ-BUILD path which in turn contains the MAJIQ-PSI files used for to compile the splicing modules.
 
 [DEFAULT] This is the study global information needed for the analsysis. The mandatory fields are:
- build_path: Directory to the MAJIQ-build containing the MAJIQ-PSI output files under the subdir /psi.
- genelist: List of gene names for which to build splicing modules
- refgroup: reference group from which to use junction information to build the splicing modules. Must be a key found in [GROUPS].
- qr_thresh: quantification ratio (qr) threshold defined as the number of samples where a distinct lsv is quantified dived by the total nnubmer of samples in that group. Default value = 0.8
- splicedb: Name of excel file to use as the input splicing module database. The file is initialized and populated with the MakeSplicingModuleDatabase.py script.
 
 [GROUPS] Section to define experimental groups. The key is the group name and element is a text file containing the sample names to use to construct a particular group. 

 
####  Compile Database of Splicing Modules
Parse through each input .psi file for each sample across each defined group and generate a table of the most variable splicing modules.
```bash
python ./src/MakeSplicingModuleDatabase.py -c CONFIG_FILE
```

The above function exports an excel file with splicing modules as the indexing feature. The table contains the following feature set:
- MODULE_ID: Unique identifier of the splicing module generated from the input MAJIQ-build database.
- JUNCTION_ID: Genomic coordinates of the junction that defines the splicing module.
- GENE_ID: Ensembl ID of the gene containing the splicing module.
- GENE_NAME: Common name (HUGO symbol) of the gene containing the splicing module. 
- STRAND: Genomic strand location of the splicing module.
- J(LSV): LSV from which the junction defining the splicing module originated.
- SPLICING_MODULE: List of LSVs that compose the splicing module (1 or 2 LSVs)
- MODULE_JUNCTIONS: List of genomic coordinates for all the junctions that compose a splicing module.
- EXONS: List of genomic coordinates for the exons that compose the splicing module.

Group-specific features:
- MAX_VAR_{group}: Most variable junction
- QR_{group}: Quantification ratio of the most variable junction in the splicing module for the specified group
- VARIANCE_{group}: Variance of most variable junction in the splicing module for the specified group
- STDEV_{group}: Standard deviation of most variable junction in the splicing module for the specified group

In particular instances, you may wish to also interact with the database and extract relevant in formation. 

The splicing module database only contains quantification information of the most vairable junction for a particular splicing module. For more comprehensive studies of splicing variability, one may wish to explore the quantification of all the junctions and LSVs that compose a given splicing module. Use the following function to query the LSV-level and junction-level data underlying a splicing module and export a table of the psi data pertaining to all the junctions composing the splicing module.
```bash
python ./src/UnpackSplicingModule.py -c CONFIG_FILE -sm MODULE_ID 
```
- CONFIG_FILE : configuration file used to make splicing module database from which the [module_id] originated from.
- MODULE_ID : string of the [module_id] to query form the splicing module databse 

Depending on how the experiment has been designed, there may be a time when you wish to query a set of splicing modules within a particular database within a separately built MAJIQ-PSI databse that may represent a totally different group of samples. Given that splicing modules are constructed dynamically based on the sample groups that compose a MAJIQ-PSI database, the query must be done using the junction coordinates that represent the splicing modules of interest. Use the following function to query a set of defined splicing modules within a separately built MAJIQ database.
```bash
python ./src/QuerySplicingModuleDatabase.py -c CONFIG_FILE -moi SPLICING_MODULES
```
- CONFIG_FILE : configuration file used to make splicing module database from which the [module_id] originated from.
- SPLICING_MODULES : a splicing module table that is outputted by the [MakeSplicingModuleDatabase.py] script 
 
#### Pairwise Correlation Splicing Modules
Correlate a set of splicing modules in a pairwise fashion per sample group defined in a specified CONFIG_FILE. The correlation matrix is initialized using the .psi value vectors of the reference group. The script outputs a distance matrix of the correlations between the defined axis. The default axis is MODULE_ID and thus calculates pairwise comparisons of between each of the splicing modules in a defined database. If "culstax" is defined to "samples" the script calculates pairwise comparison between all the samples in the group.
```bash
# Make distance matrix
python ./src/SplicingModuleCoregulation.py -c CONFIG_FILE -m METHOD -i MODULE_ID
```

## Correlating splicing module data with gene expression.
As part of your integrative analysis of mRNA-seq data, you may wish to correlate the splicing quantification data with gene expression data. To add this capability, add the list of gene expression dataframes into the config file under the [XSETS] section. The script will use the column names in the gene expression dataframe to query the samples in the designated splicing module database. 

#### Correlate a splicing modules in a database with the expression of each of the pertaining genes.
For each module _m_, correlate a vector of PSI values representative of splicing module _m_ in gene _g_ with a vector of relative expression for that same gene _g_ across the distinct sample groups devined in the [GROUPS] section.
```bash
python ./src/CorrSplicingModulesWithExpression.py
```

#### Correlate a single splicing module with the expression of genes in a particular geneset
For a module _m_, correlate the vector of PSI values with the gene expression of every gene _g_ in a geneset of interest. Determine how many of the genes of the geneset are expressed above a defined threshold (default=1) and use the number of genes _n_ to construct a random geneset of length _n_ 
```bash
python ./src/CorrSplicingModuleWithGeneset.py -c CONFIG_FILE -sm MODULE_ID -g GENESET -n NAME
```
- GENESET : A .csv file with at least two columns GENE_ID and GENE_NAME.
- NAME : A string denoting the name of the geneset for naming the output file.

## III. Appendix
#### Data Visualization
Data Visualization of steps currently within Rmarkdown notebook ./docs/Rivera2021.Rmd and will soon be implemented as Rscripts.

#### Gene Expression Quantification
Concatenation of SALMON .quant_sf files into gene expression count matrices for yeah group can be found under /docs/GeneExpressionQuantification.Rmd